﻿using FGHZ.Entity;
using FGHZ.Entity.Objects;
using FGHZ.Entity.Units.Enemies;
using FGHZ.Entity.Units.Player;
using System.Collections.Generic;

namespace FGHZ.Engine
{
    class LevelData
    {
        public Player player;
        public List<IndestructableEntity> indestructables;
        public List<DestructableObject> destructables;
        public List<BaseEnemy> enemies;
        public readonly int HEIGHT, WIDTH;

        public LevelData(int nHeight, int nWidth)
        {
            HEIGHT = nHeight;
            WIDTH = nWidth;

            indestructables = new List<IndestructableEntity>();
            destructables = new List<DestructableObject>();
            enemies = new List<BaseEnemy>();
        }
    }
}
