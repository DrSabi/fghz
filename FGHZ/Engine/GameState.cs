﻿namespace FGHZ.Engine
{
    public enum GameState
    {
        Normal,
        Endless,
        Custom,
        Exit,
        Won,
        Lost
    }
}
