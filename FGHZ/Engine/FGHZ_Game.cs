﻿using FGHZ.Entity;
using FGHZ.Entity.Objects;
using FGHZ.Entity.Units.Enemies;
using System.Collections.Generic;
using System;

namespace FGHZ.Engine
{
    class FGHZ_Game
    {
        private const int FPS = 25;
        private const int HEIGHT = 65;
        private const int WIDTH = 125;

        private Tilemap map;
        private Renderer renderer;

        private GameState gameState;
        private int nLevel = 0;
        private List<LevelData> levelData;
        private Tilemap baseMap;

        public FGHZ_Game()
        {
            map = new Tilemap(HEIGHT, WIDTH);
            renderer = new Renderer(map);
            baseMap = new Tilemap(HEIGHT, WIDTH);
        }

        private void InitNormalLevels()
        {
            LevelFileHandler lvlHandler = new LevelFileHandler(".\\Levels\\builtin\\normal", HEIGHT, WIDTH);
            levelData = lvlHandler.ReadFolder();
        }

        private void InitEndlessLevel()
        {
            LevelFileHandler lvlHandler = new LevelFileHandler(".\\Levels\\builtin\\endless", HEIGHT, WIDTH);
            levelData = lvlHandler.ReadFolder();
        }

        private void InitCustomLevels()
        {
            LevelFileHandler lvlHandler = new LevelFileHandler(".\\Levels\\custom\\", HEIGHT, WIDTH);
            levelData = lvlHandler.ReadFolder();
        }

        public void Run()
        {
            do
            {
                renderer.ResetBuffer();
                ShowMenu();

                switch (this.gameState)
                {
                    case GameState.Normal:
                        InitNormalLevels();
                        break;
                    case GameState.Endless:
                        InitEndlessLevel();
                        break;
                    case GameState.Custom:
                        InitCustomLevels();
                        break;
                }

                if (this.gameState == GameState.Exit)
                    return;

                GameLoop();

                ShowEndScreen();
            } while (gameState != GameState.Exit);
        }

        private void ShowEndScreen()
        {
            Console.SetCursorPosition(0, 0);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();

            if (gameState == GameState.Won)
                Console.WriteLine("You won!");
            else if (gameState == GameState.Lost)
                Console.WriteLine("You lost!");

            Console.WriteLine("[Enter] to return to the Menu");
            Console.ReadLine();
        }

        private void ShowNextLevelScreen(int nLvl)
        {
            Console.SetCursorPosition(0, 0);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();

            Console.WriteLine("You beat Level " + nLvl + "!");

            Console.WriteLine("[Enter] to get to the next level");
            Console.ReadLine();

            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
        }

        private void ShowMenu()
        {
            Console.Clear();
            Console.WriteLine("Select Gamemode: ");
            Console.WriteLine("-----------------");
            Console.WriteLine("1. Normal");
            Console.WriteLine("2. Endless");
            Console.WriteLine("3. Custom");
            Console.WriteLine("0. Exit");
            Console.WriteLine("------------------------");
            String choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    this.gameState = GameState.Normal;
                    break;
                case "2":
                    this.gameState = GameState.Endless;
                    break;
                case "3":
                    this.gameState = GameState.Custom;
                    break;
                default:
                    this.gameState = GameState.Exit;
                    break;
            }

            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
        }

        private void GameLoop()
        {
            System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
            ConsoleKey? key = null;

            stopWatch.Start();
            Console.CursorVisible = false;

            nLevel = 0;
            LevelData lvl = levelData[nLevel];

            //renderer.UpdateScreenBufferedArea(0, 0);
            while (key != ConsoleKey.Escape && (gameState != GameState.Won && gameState != GameState.Lost))
            {
                if (lvl != levelData[nLevel])
                {
                    renderer.ResetBuffer();
                    lvl = levelData[nLevel];
                }

                // Player-actions
                lvl.player.Update(lvl, key);

                // Update enemies and objects
                UpdateLevelEntities(lvl, key);

                // Draw enemies and objects
                DrawLevelEntities(lvl);

                // draw player
                lvl.player.Draw(map);

                // Drawing Tilemap on screen
                renderer.DrawMovingScreen(lvl.player);

                // Reset Tilemap to level-default
                map.Reset(baseMap);

                // Wait for FSP-Cap
                WaitForFPS(stopWatch);

                // Read new KeyPresses
                key = Console.KeyAvailable ? Console.ReadKey(true).Key : (ConsoleKey?)null;

                // Check for game-ending condition
                CheckWin(lvl, ref nLevel);
            }
        }

        private void CheckWin(LevelData lvl, ref int nLevel)
        {
            // Player lost:
            if (lvl.player.IsDestroyed())
            {
                gameState = GameState.Lost;
            }
            else if (gameState != GameState.Endless) // Player won / level done:
            {
                if (lvl.enemies.Count == 0) // Player won
                { 
                    if (nLevel < levelData.Count - 1) // Next Level
                    {
                        ShowNextLevelScreen(nLevel+1);
                        nLevel += 1;
                    }
                    else // All levels won
                        gameState = GameState.Won;
                }
            }
        }

        private void UpdateLevelEntities(LevelData lvl, ConsoleKey? key)
        {
            // Enemy-actions
            for (int i = lvl.enemies.Count - 1; i >= 0; i--)
            {
                BaseEnemy e = lvl.enemies[i];
                e.Update(lvl, key);
                if (e.IsDestroyed())
                {
                    lvl.enemies.RemoveAt(i);
                    if (gameState == GameState.Endless)
                    {
                        Random rnd = new Random();
                        int nextEnemyType = rnd.Next(0, 2);
                        switch (nextEnemyType)
                        {
                            case 1:
                                lvl.enemies.Add(new Enemy_strong(lvl));
                                break;
                            case 2:
                                lvl.enemies.Add(new Enemy_tank(lvl));
                                break;
                            default:
                                lvl.enemies.Add(new Enemy_normal(lvl));
                                break;
                        }
                    }
                }
            }

            // Object-actions
            foreach (IndestructableEntity e in lvl.indestructables)
                e.Update(lvl, key);
            for (int i = lvl.destructables.Count - 1; i >= 0; i--)
            {
                DestructableObject e = lvl.destructables[i];
                e.Update(lvl, key);
                if (e.IsDestroyed())
                    lvl.destructables.RemoveAt(i);
            }
        }

        private void DrawLevelEntities(LevelData lvl)
        {
            // Drawing Entities on the Tilemap
            foreach (IndestructableEntity e in lvl.indestructables)
                e.Draw(map);
            foreach (DestructableObject e in lvl.destructables)
                e.Draw(map);
            foreach (BaseEnemy e in lvl.enemies)
                e.Draw(map);
        }

        private void WaitForFPS(System.Diagnostics.Stopwatch stopWatch)
        {
            float timeToWait;

            timeToWait = 1000 / FPS;
            timeToWait -= stopWatch.ElapsedMilliseconds;

            if (timeToWait > 0)
                System.Threading.Thread.Sleep((int)timeToWait);

            stopWatch.Restart();
        }
    }
}