﻿using System;
using FGHZ.Engine;
using FGHZ.Entity.EntityTools;
using FGHZ.Entity.Objects;
using FGHZ.Entity.Units.Enemies;

namespace FGHZ.Entity
{
    abstract class BaseEntity
    {
        protected Position pos;
        protected readonly HitBox hitbox;

        public BaseEntity(int nPosY, int nPosX, int nSizeY, int nSizeX)
        {
            pos = new Position(nPosY, nPosX);
            hitbox = new HitBox(nSizeY, nSizeX);
        }

        // Constructor for random placement
        public BaseEntity(int nSizeY, int nSizeX, LevelData lvl)
        {
            hitbox = new HitBox(nSizeY, nSizeX);

            Random rng = new Random();
            Position newPos;
            int nPosY, nPosX;
            do
            {
                nPosY = rng.Next(0, lvl.HEIGHT - nSizeY - 1);
                nPosX = rng.Next(0, lvl.WIDTH - nSizeX - 1);

                newPos = new Position(nPosY, nPosX);

            } while (!IsValidPosition(newPos, lvl));

            pos = newPos;
        }

        private bool IsValidPosition(Position pos, LevelData lvl)
        {
            if (hitbox.DoCollide(lvl.player.hitbox, pos, lvl.player.pos))
                return false;

            foreach (BaseEnemy e in lvl.enemies)
                if (hitbox.DoCollide(e.hitbox, pos, e.pos))
                    return false;

            foreach (DestructableObject e in lvl.destructables)
                if (hitbox.DoCollide(e.hitbox, pos, e.pos))
                    return false;

            foreach (IndestructableEntity e in lvl.indestructables)
                if (hitbox.DoCollide(e.hitbox, pos, e.pos))
                    return false;

            return true;
        }

        public double GetDistance(BaseEntity e)
        {
            double p1Y = pos.PosY + hitbox.MiddleY;
            double p1X = pos.PosX + hitbox.MiddleX;
            double p2Y = e.pos.PosY + e.hitbox.MiddleY;
            double p2X = e.pos.PosX + e.hitbox.MiddleX;

            return Math.Sqrt(Math.Pow(p1Y - p2Y, 2) + Math.Pow(p1X - p2X, 2));
        }

        public Position GetPosition()
        {
            return pos;
        }

        public HitBox GetHitbox()
        {
            return hitbox;
        }

        public abstract void Draw(Tilemap map);

        public abstract void Update(LevelData data, ConsoleKey? keyPressed);
    }
}
