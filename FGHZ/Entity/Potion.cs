﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FGHZ.Entity
{
    class Potion
    {
        readonly float fHealing;
        readonly ConsoleColor color;

        public ConsoleColor Color { get => color; }
        public float Healing { get => fHealing; }

        public Potion(float healing)
        {
            fHealing = healing;
            if (fHealing < 10)
                color = ConsoleColor.Yellow;
            else
                color = ConsoleColor.Green;
        }
    }
}
