﻿using FGHZ.Engine;
using FGHZ.Entity.EntityTools;
using FGHZ.Entity.Units.Enemies;
using System;
using System.Collections.Generic;

namespace FGHZ.Entity.Units
{
    abstract class Unit : DestructableEntity
    {
        List<Equipment> equipment = new List<Equipment>();
        Equipment effective_equipment;

        public Unit (int nPosY, int nPosX, int nSizeY, int nSizeX, float maxHP, Equipment start) : base(nPosY, nPosX, nSizeY, nSizeX, maxHP)
        {
            AddEquipment(start);
           
        }

        // Constructor for random placement
        public Unit(int nSizeY, int nSizeX, float maxHP, Equipment start, LevelData data) : base(nSizeY, nSizeX, maxHP, data)
        {
            AddEquipment(start);

        }

        public void AddEquipment(Equipment e)
        {
            equipment.Add(e);
            RecalcEffEquipment();
        }

        private void RecalcEffEquipment()
        {
            float count = 0;
            float rangeSum = 0;
            effective_equipment = new Equipment(0, 0, 0);

            foreach (Equipment e in equipment)
            {
                if (e.fRange != 0)
                {
                    count++;
                }
                effective_equipment.fDmg += e.fDmg;
                effective_equipment.fArmor += e.fArmor;
                rangeSum += e.fRange;
            }
            // average of the equipment that gives range
            effective_equipment.fRange = rangeSum / count;
        }

        protected float GetAttack()
        {
            return effective_equipment.fDmg;
        }

        protected float GetArmor()
        {
            return effective_equipment.fArmor;
        }

        protected float GetRange()
        {
            return effective_equipment.fRange;
        }

        protected bool Move(int MoveY, int MoveX, LevelData data)
        {
            Position newPos = new Position(pos.PosY + MoveY, pos.PosX + MoveX);

            //collision with world border
            if (pos.PosY + MoveY + this.GetHitbox().SizeY> data.HEIGHT || pos.PosY + MoveY < 0)
            {
                return false;
            }

            //collision with world border
            if (pos.PosX + MoveX + this.GetHitbox().SizeX > data.WIDTH || pos.PosX + MoveX < 0)
            {
                return false;
            }

            //collision with wall
            foreach (IndestructableEntity i in data.indestructables)
            {
                if (hitbox.DoCollide(i.GetHitbox(), newPos, i.GetPosition()))
                {
                    return false;
                }
            }

            //collision with object
            foreach (DestructableEntity d in data.destructables)
            {
                if (hitbox.DoCollide(d.GetHitbox(), newPos, d.GetPosition()))
                {
                    return false;
                }
            }

            //collision with enemy
            foreach (BaseEnemy e in data.enemies)
            {
                if (this != e)
                {
                    if (hitbox.DoCollide(e.GetHitbox(), newPos, e.GetPosition()))
                    {
                        return false;
                    }
                }
            }

            if (this != data.player)
            {
                if (hitbox.DoCollide(data.player.GetHitbox(), newPos, data.player.GetPosition()))
                    {
                        return false;
                    }
            }

            pos.PosY += MoveY;
            pos.PosX += MoveX;
            return true;
        }


        protected bool Attack(DestructableEntity e, bool isObject)
        {
            if (isObject)
            {
                if (GetDistance(e) < effective_equipment.fRange)
                {
                    e.DecreaseHealth(effective_equipment.fDmg); 
                    return true;
                }
            }
            else
            {
                Unit u = (Unit)e;
                if (GetDistance(u) < effective_equipment.fRange)
                {
                    if (effective_equipment.fDmg - u.GetArmor() > 0)
                    {
                        u.DecreaseHealth(effective_equipment.fDmg - u.GetArmor());
                        return true;
                    }
                    //minimum dmg 1
                    else
                    {
                        u.DecreaseHealth(1);
                        return true;
                    }
                }
            }

            return false;
        }

        protected void Recoil(Unit u, LevelData data, bool isDefending)
        {
            double playerX = pos.PosX + hitbox.MiddleX;
            double playerY = pos.PosY + hitbox.MiddleY;

            double enemyX = u.pos.PosX + u.hitbox.MiddleX;
            double enemyY = u.pos.PosY + u.hitbox.MiddleY;

            double xDist = playerX - enemyX;
            double yDist = playerY - enemyY;

            if (GetDistance(u) < effective_equipment.fRange)
            {
                if (!isDefending)
                {
                    //calc where the pos of enemy is relative to you to see which direction the recoil is
                    if (Math.Abs(xDist) > Math.Abs(yDist))
                    {
                        if (xDist < 0)
                        {
                            u.Move(0, 1, data); //move once three times to avoid clipping through walls
                            u.Move(0, 1, data);
                            u.Move(0, 1, data);
                        }

                        else
                        {
                            u.Move(0, -1, data);
                            u.Move(0, -1, data);
                            u.Move(0, -1, data);
                        }

                    }
                    else
                    {
                        if (yDist < 0)
                        {
                            u.Move(1, 0, data);
                            u.Move(1, 0, data);
                            u.Move(1, 0, data);

                        }

                        else
                        {
                            u.Move(-1, 0, data);
                            u.Move(-1, 0, data);
                            u.Move(-1, 0, data);
                        }
                    }
                }
            }
        }
    }
}
