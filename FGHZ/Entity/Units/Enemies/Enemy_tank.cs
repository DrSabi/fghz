﻿
using FGHZ.Engine;
using FGHZ.Entity.Objects;
using FGHZ.Entity.EntityTools;
using FGHZ.Entity.Units.Player;
using System;
using System.Collections.Generic;
using System.Text;

namespace FGHZ.Entity.Units.Enemies
{
    class Enemy_tank : BaseEnemy
    {
        static readonly Tile rBeak = new Tile('>', System.ConsoleColor.DarkRed);
        static readonly Tile lBeak = new Tile('<', System.ConsoleColor.DarkRed);
        static readonly Tile eyes = new Tile('"', System.ConsoleColor.DarkRed);
        static readonly Tile rHead = new Tile('(', System.ConsoleColor.DarkRed);
        static readonly Tile lHead = new Tile(')', System.ConsoleColor.DarkRed);
        static readonly Tile rStomach = new Tile(')', System.ConsoleColor.DarkRed);
        static readonly Tile lStomach = new Tile('(', System.ConsoleColor.DarkRed);
        static readonly Tile rBack = new Tile('\u002F', System.ConsoleColor.DarkRed);
        static readonly Tile lBack = new Tile('\u005C', System.ConsoleColor.DarkRed);
        static readonly Tile lWeapon = new Tile('\u03A8', System.ConsoleColor.DarkRed);
        static readonly Tile rWeapon = new Tile('\u03A8', System.ConsoleColor.DarkRed);
        static readonly Tile rShild = new Tile('|', System.ConsoleColor.DarkRed);
        static readonly Tile lShild = new Tile('|', System.ConsoleColor.DarkRed);
        static readonly Tile rDef = new Tile('\u002F', System.ConsoleColor.DarkRed);
        static readonly Tile lDef = new Tile('\u005C', System.ConsoleColor.DarkRed);
        static readonly Tile downEyes = new Tile('"', System.ConsoleColor.DarkRed);

        const int Y_SIZE = 2;
        const int X_SIZE = 3;
        const int HP = 10;
        static readonly Equipment start = new Equipment(3, 3, 3);
        static readonly Equipment extraPlus = new Equipment(5, 0, 0);
        static readonly Equipment extraMinus = new Equipment(-5, 0, 0);

    

        System.Diagnostics.Stopwatch move_watch = new System.Diagnostics.Stopwatch();
        System.Diagnostics.Stopwatch attack_watch = new System.Diagnostics.Stopwatch();
        const int WAIT_TIME_MOVE = 500, WAIT_TIME_ATTACK = 500;

        public Enemy_tank(int nPosY, int nPosX) : base(nPosY, nPosX, Y_SIZE, X_SIZE, HP, start)
        {
            move_watch.Start();
            attack_watch.Start();
        }

        // Constructor for random placement
        public Enemy_tank(LevelData data) : base(Y_SIZE, X_SIZE, HP, start, data)
        {
            move_watch.Start();
            attack_watch.Start();
        }

        public override void Draw(Tilemap map)
        {
            if (lookingLeft)
            {
                if (!Defending)
                {
                    map.SetTile(pos.PosY, pos.PosX, lBeak);
                    map.SetTile(pos.PosY, pos.PosX + 1, eyes);
                    map.SetTile(pos.PosY, pos.PosX + 2, lHead);
                    map.SetTile(pos.PosY + 1, pos.PosX, rHead);
                    map.SetTile(pos.PosY + 1, pos.PosX + 1, lStomach);
                    map.SetTile(pos.PosY + 1, pos.PosX + 2, lBack);
                }
                else
                {
                    map.SetTile(pos.PosY, pos.PosX, lBeak);
                    map.SetTile(pos.PosY, pos.PosX + 1, downEyes);
                    map.SetTile(pos.PosY, pos.PosX + 2, lHead);
                    map.SetTile(pos.PosY + 1, pos.PosX, lShild);
                    map.SetTile(pos.PosY + 1, pos.PosX + 1, lDef);
                    map.SetTile(pos.PosY + 1, pos.PosX + 2, lHead);
                }
            }
            else
            {
                if (!Defending) 
                {
                    map.SetTile(pos.PosY, pos.PosX, rHead);
                    map.SetTile(pos.PosY, pos.PosX + 1, eyes);
                    map.SetTile(pos.PosY, pos.PosX + 2, rBeak);
                    map.SetTile(pos.PosY + 1, pos.PosX, rBack);
                    map.SetTile(pos.PosY + 1, pos.PosX + 1, rStomach);
                    map.SetTile(pos.PosY + 1, pos.PosX + 2, lHead);
                }
                else
                {
                    map.SetTile(pos.PosY, pos.PosX, rHead);
                    map.SetTile(pos.PosY, pos.PosX + 1, downEyes);
                    map.SetTile(pos.PosY, pos.PosX + 2, rBeak);
                    map.SetTile(pos.PosY + 1, pos.PosX, rHead);
                    map.SetTile(pos.PosY + 1, pos.PosX + 1, rDef);
                    map.SetTile(pos.PosY + 1, pos.PosX + 2, rShild);
                }
            }
        }

        public override void Update(LevelData data, ConsoleKey? keyPressed)
        {
            if (move_watch.ElapsedMilliseconds < WAIT_TIME_MOVE)
            {
                return;
            }
            else
            {
                if (GetDistance(data.player) <= 17)
                {
                    if (GetDistance(data.player) <= 7 && !Defending)
                    {

                        AddEquipment(extraPlus);

                        Defending = true;
                    }
                    else
                    {
                        if (GetDistance(data.player) >= 8)
                        {
                            if (Defending)
                            {
                                Defending = false;
                                AddEquipment(extraMinus);
                            }

                            MoveToPlayer(data);
                        }

                    }
                }
                else
                {
                    RandomWalk(data);
                }
                move_watch.Restart();
            }

            if (attack_watch.ElapsedMilliseconds < WAIT_TIME_ATTACK)
            {
                return;
            }
            else
            {
                Attack(data.player, false);
                attack_watch.Restart();
            }
        }
    }
}