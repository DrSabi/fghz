﻿namespace FGHZ.Entity.Objects
{
    abstract class DestructableObject : DestructableEntity
    {
        public DestructableObject(int nPosY, int nPosX, int nSizeY, int nSizeX, float fHP) : base(nPosY, nPosX, nSizeY, nSizeX, fHP) { }
    }
}
