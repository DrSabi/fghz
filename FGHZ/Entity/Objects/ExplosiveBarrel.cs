﻿using System;
using FGHZ.Engine;

namespace FGHZ.Entity.Objects
{
    class ExplosiveBarrel : DestructableObject
    {
        static int barrelStartHP = 1;
        int range = 10;
        int dmg = 5;
        static readonly Tile upperLeft = new Tile('O', ConsoleColor.Magenta);

        public ExplosiveBarrel(int nPosY, int nPosX) : base(nPosY, nPosX, 2, 2, barrelStartHP) { }

        public override void Draw(Tilemap map)
        {
            int y = pos.PosY;
            int x = pos.PosX;
            map.SetTile(y, x, upperLeft);
            map.SetTile(y + 1, x, upperLeft);
            map.SetTile(y, x + 1, upperLeft);
            map.SetTile(y + 1, x + 1, upperLeft);
        }

        public override void Update(LevelData data, ConsoleKey? keyPressed)
        {
            if(this.fHP <= 0)
            {
                foreach(DestructableEntity d in data.destructables)         //damages all destructable objects in range
                {
                    if (this.GetDistance(d) < range)
                    {
                        d.DecreaseHealth(dmg);
                    }
                }
                foreach(DestructableEntity e in data.enemies)
                {
                   if (this.GetDistance(e) < range)
                   {
                       e.DecreaseHealth(dmg);
                   }
                }
                if(this.GetDistance(data.player) < range)
                {
                   data.player.DecreaseHealth(dmg);
                }
            }
        }
    }
}
